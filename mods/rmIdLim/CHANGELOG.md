# Changelog

All notable changes to this project will be documented in this file.

## [0.2] - 2022-07-05
### Added
 - This changelog.
 - The limit modifiers can be now accessed from the config file.

## [0.1] - 2022-07-01
### Added
 - Initial release of the mod.