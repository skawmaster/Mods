extends Node

func _init(modLoader = ModLoader):
	Debug.l("rmIdLim: Initializing")
	modLoader.installScriptExtension("res://rmIdLim/hud/AutopilotOverlay.gd")
	Debug.l("rmIdLim: Finished initialization")