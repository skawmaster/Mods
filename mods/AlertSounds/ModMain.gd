extends Node

# Ensure this runs before any mods which cause ships/Shipyard.gd to be
# loaded, as it loads ship .tscn files which bind the original
# ship-ctrl.gd, not giving us a chance to replace it.
const MOD_PRIORITY = -1

func _init(modLoader = ModLoader):
	Debug.l("ALERT-SOUNDS: Initializing")
	modLoader.installScriptExtension("res://AlertSounds/ships/ship-ctrl.gd")
