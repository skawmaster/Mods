extends "res://ships/ship-ctrl.gd"

var _alertSounds_default
var _alertSounds_lowFuel
var _alertSounds_collision

func _load_pcm(path):
	var file = File.new()
	file.open(path, File.READ)
	var buffer = file.get_buffer(file.get_len())
	file.close()

	var stream = AudioStreamSample.new()
	stream.format = AudioStreamSample.FORMAT_16_BITS
	stream.mix_rate = 44100
	stream.stereo = false
	stream.data = buffer

	return stream

func _ready():
	Debug.l("ship-ctrl _ready")
	if isPlayerControlled():
		_alertSounds_default = playerAlert.stream
		_alertSounds_lowFuel = _load_pcm("res://AlertSounds/low-fuel.pcm")
		_alertSounds_collision = _load_pcm("res://AlertSounds/collision.pcm")

func soundAlert():
	var stream
	match get_stack()[1]["source"]:
		"res://hud/AutopilotOverlay.gd":
			stream = _alertSounds_collision
		"res://hud/circularReadout.gd":
			stream = _alertSounds_lowFuel
		_:
			stream = _alertSounds_default
	if playerAlert != null and playerAlert.stream != stream:
		Debug.l("ALERT-SOUNDS: Playing " + str(stream))
		playerAlert.stream = stream
	.soundAlert()
