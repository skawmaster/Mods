extends Node

func _init(modLoader = ModLoader):
	Debug.l("HEAVY-COTHON: Initializing")

	modLoader.installScriptExtension('res://HeavyCothon/ships/Shipyard.gd')
	modLoader.installScriptExtension('res://HeavyCothon/CurrentGame.gd')

	modLoader.addTranslationsFromCSV("res://HeavyCothon/translation.csv")

	Debug.l("HEAVY-COTHON: Initialized")

func _ready():
	Debug.l("HEAVY-COTHON: Readying")

	var scene = preload("res://enceladus/Dealer.tscn")
	var modifiedScene = scene.instance()
	ModLoader.appendNodeInScene(modifiedScene, "HEAVY-COTHON", "VP/Viewport", "res://ships/dealer/showroom-Cothon.tscn", false)
	ModLoader.saveScene(modifiedScene, "res://enceladus/Dealer.tscn")

	Debug.l("HEAVY-COTHON: Ready")
