extends Node

const MOD_PRIORITY = -1

func _init(modLoader = ModLoader):
	modLoader.installScriptExtension("res://TogglePropulsion/Settings.gd")
	modLoader.installScriptExtension("res://TogglePropulsion/ships/ship-ctrl.gd")
