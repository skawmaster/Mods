extends "res://ships/ship-ctrl.gd"

func _unhandled_input(event):
	var type
	if Input.is_action_just_pressed("toggle_rcs"):
		type = "propulsion.rcs"
	elif Input.is_action_just_pressed("toggle_engines"):
		type = "propulsion.main"
	if isPlayerControlled() && !cutscene:
		var have_enabled = 0
		var have_disabled = 0
		for c in get_children():
			if "type" in c and c.type == type:
				if c.enabled:
					have_enabled += 1
				else:
					have_disabled += 1
		var enable = have_enabled < have_disabled
		for c in get_children():
			if "type" in c and c.type == type:
				c.enabled = enable
