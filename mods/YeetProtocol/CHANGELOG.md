# Changelog

All notable changes to this project will be documented in this file.

## [0.1] - 2022-08-22
### Added
 - Initial release of the mod based on 0.549.2 version.