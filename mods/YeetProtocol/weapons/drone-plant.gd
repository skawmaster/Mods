extends "res://weapons/drone-plant.gd"

func scanForTarget(delta):
	scanProcess += delta
	if scanProcess > scanEvery:
		var dmin = getRangeMin() * 10
		var dmax = getRangeMax() * 10
		scanProcess = 0
		target = null
		var space = ship.get_parent()
		var priority = false
		var previousTarget = target
		match droneFunction:
			"haul":
				if ship.droneQueue:
					for t in ship.droneQueue:
						if Tool.claim(t):
							if t == ship.autopilotVelocityOffsetTarget:
								Tool.release(t)
								continue
							var dist = global_position.distance_to(t.global_position)
							if dist > laserRange:
								Tool.release(t)
								continue
							var desired = desiredVelicityFor(t)
							var check = t.mass * pow((desired - t.linear_velocity).length(), 2) / dist
							if t == previousTarget:
								check *= currentTargetBias
							if check > minEnergyToTarget:
								target = t
								Tool.release(t)
								priority = true
								break
							Tool.release(t)
								
				if not priority:
					var best = minEnergyToTarget
					for t in getPossibleTargets():
						if Tool.claim(t):
							if t.get_parent() != space:
								Tool.release(t)
								continue
							if "inCargoHold" in t and t.inCargoHold:
								Tool.release(t)
								continue
							if not t is RigidBody2D:
								Tool.release(t)
								continue
							if t == ship.autopilotVelocityOffsetTarget:
								Tool.release(t)
								continue
							var dist = global_position.distance_to(t.global_position)
							if dist > dmax or dist < dmin:
								Tool.release(t)
								continue
							if sanbus and not ship.isValidSystemTarget(self, t):
								Tool.release(t)
								continue
							var desired = desiredVelicityFor(t)
							if mineralTargetting and not ship.isValidMineralTarget(t, mineralConfig):
								desired = desiredYeetVelocityFor(t)
								
							var check = t.mass * pow((desired - t.linear_velocity).length(), 2) / dist
							check *= clamp(float(ship.isValidMineralTarget(t, mineralConfig)), 0.25, 1.0)
							if t == previousTarget:
								check *= currentTargetBias
							if check > best:
								target = t
								best = check
							Tool.release(t)
			"tug":
				if ship.droneQueue:
					for t in ship.droneQueue:
						if Tool.claim(t):
							var dist = global_position.distance_to(t.global_position)
							if dist > laserRange:
								Tool.release(t)
								continue
							var v = t.linear_velocity.length()
							if v < minVelocityToTarget:
								Tool.release(t)
								continue
							var check = t.mass * pow(v, 2)
							if t == previousTarget:
								check *= currentTargetBias
							if check > minEnergyToTarget:
								target = t
								priority = true
								Tool.release(t)
								break
							Tool.release(t)
				if not priority:
					var best = minEnergyToTarget
					for t in getPossibleTargets():
						if Tool.claim(t):
							if t.get_parent() != space:
								Tool.release(t)
								continue
							if "inCargoHold" in t and t.inCargoHold:
								Tool.release(t)
								continue
							if not t is RigidBody2D:
								Tool.release(t)
								continue
							var dist = global_position.distance_to(t.global_position)
							if dist > dmax or dist < dmin:
								Tool.release(t)
								continue
							var v = t.linear_velocity.length()
							if v < minVelocityToTarget:
								Tool.release(t)
								continue
							if sanbus and not ship.isValidSystemTarget(self, t):
								Tool.release(t)
								continue
							if mineralTargetting and not ship.isValidMineralTarget(t, mineralConfig):
								Tool.release(t)
								continue
							var check = t.mass * pow(v, 2)
							if t == previousTarget:
								check *= currentTargetBias
							if check > best:
								target = t
								best = check
							Tool.release(t)
			"repair":
				var systems = ship.getSystems()
				for k in systems:
					var system = systems[k]
					for d in system.damage:
						if d.current > d.maxRaw * (1 - repairLimit):
							target = ship
							return 
	else :
		if Tool.claim(target):
			var rel = false
			if "inCargoHold" in target and target.inCargoHold:
				rel = true
			Tool.release(target)
			if rel:
				target = null
		

func desiredYeetVelocityFor(target:RigidBody2D)->Vector2:
	var toShip = ship.global_position - target.global_position
	var vAwayFromShip = ship.linear_velocity - toShip.normalized() * haulVelocity * 3.0
	return vAwayFromShip


func _on_DroneLaunchManager_droneHit(pt, delta, drones):
	match droneFunction:
		"haul":
			if Tool.claim(target):
				if target.global_position != null:
					var distance = pt.distance_to(target.global_position)
					if distance < hitDeadZone:
						var desired = Vector2(0,0)
						if ship.isValidMineralTarget(target, mineralConfig):
							desired = desiredVelicityFor(target)
						else: 
							desired = desiredYeetVelocityFor(target)
						
						var impulse = (desired - target.linear_velocity).normalized() * delta * drones * droneTugPower
						
						target.apply_impulse(Vector2(0, 0), impulse)
					else :
						Debug.l("Haul drone target out of range: %f" % distance)
				Tool.release(target)
			
		"tug":
			if Tool.claim(target):
				if target.global_position != null:
					var distance = pt.distance_to(target.global_position)
					if distance < hitDeadZone:
						var impulse = - target.linear_velocity.normalized() * delta * drones * droneTugPower
						
						target.apply_impulse(Vector2(0, 0), impulse)
					else :
						Debug.l("Tug drone target out of range: %f" % distance)
				Tool.release(target)
		"repair":
			if Tool.claim(target):
				if target.has_method("getSystems") and target.has_method("changeSystemDamage"):
					var distance = pt.distance_to(target.global_position)
					if distance < hitDeadZone:
						var systems = target.getSystems()
						for k in systems:
							var system = systems[k]
							for d in system.damage:
								if d.current > d.maxRaw * (1 - repairLimit):
									var fixby = clamp(fixPerDrone * delta * drones * float(d.maxRaw) / 100, 0, d.maxRaw * repairLimit)
									target.changeSystemDamage(system.key, d.type, - fixby)
					else :
						Debug.l("Maint drone target out of range: %f" % distance)
				Tool.release(target)
