extends Node

func _init(modLoader = ModLoader):
	Debug.l("YeetProtocol: Initializing")
	modLoader.installScriptExtension("res://YeetProtocol/weapons/drone-plant.gd")
	Debug.l("YeetProtocol: Finished initialization")