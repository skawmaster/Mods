YeetProtocol
===========

Yeets away the chunks which fail the geologist check.

Cleans up space. 

Warning! Will eat up quite a lot of nanodrones. oh, also you probably want to keep the Unmarked filter on. 

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/YeetProtocol.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.