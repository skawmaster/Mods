extends Node

# Ensure this runs before any mods which cause ships/Shipyard.gd to be
# loaded, as it loads ship .tscn files which bind the original
# ship-ctrl.gd, not giving us a chance to replace it.
const MOD_PRIORITY = -1

func _init(modLoader = ModLoader):
	modLoader.installScriptExtension("res://MaximumSpeed/ships/ship-ctrl.gd")
	modLoader.installScriptExtension("res://MaximumSpeed/hud/Escape Veloity.gd")
	modLoader.installScriptExtension("res://MaximumSpeed/hud/Leaving Rings.gd")

	Debug.l("MAXIMUMSPEED: Finished loading Maximum Speed mod.")
