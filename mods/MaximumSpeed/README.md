MaximumSpeed
===========

Increases the maximum allowed speed to 400m/s.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/MaximumSpeed.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.

The mod is enabled automatically.
