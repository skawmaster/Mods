BetterRefining
==============

Allows to configure all MPUs to work as you wish.

This mod has been created to satisfy my own playstyle, becouse spinning the ship to get the mineral inside the MPU smelting area breaks my immersion, so I use this mod with the MPU with the highest smelting area (Rusaton-Antonoff) to get the advantage of a nice smelting area with all the benefits of the other MPUs.

This uses the most expensive ratios for every MPU to be more balanced:
    - 90% mineralEfficiency
    - 10% emassEfficiency
    - 100 kgps
    - 1500 powerDrawPerKg
    - 0.014 dronePrintTime
    - 4.0 bulletPrintTime 

These values can be adjusted in the res://mods/BetterRefining/ships/modules/MineralProcessingUnit.gd script.

    var customMineralEfficiency = 0.9
    var customRemassEfficiency = 0.1
    var customKgps = 100
    var customPowerDrawPerKg = 1500
    var customDronePrintTime = 0.014    -> 0 to don't allow the MPU to build drones
    var customBulletPrintTime = 4.0     -> 0 to don't allow the MPU to build bullets


Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/BetterRefining.zip?job=zip).


Usage
-----
As I am a Windows/Steam user, these are the steps I followed to make it works:

1. Put the BetterRefining.zip file inside ..\steamapps\common\dV Rings of Saturn\mods 
2. Put the launching parameter --enable-mods in the Delta V application launching commands.

See the [modding documentation](../../MODDING.md) for general information.
