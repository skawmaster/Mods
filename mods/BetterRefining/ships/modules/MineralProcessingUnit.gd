extends "res://ships/modules/MineralProcessingUnit.gd"

func _physics_process(delta):
	
	var customMineralEfficiency = 0.9
	var customRemassEfficiency = 0.1
	var customKgps = 100
	var customPowerDrawPerKg = 1500
	var customDronePrintTime = 0.0
	var customBulletPrintTime = 0.0
	
	var mustBuild = false
	
	if mustBuild:
		customDronePrintTime = 0.014
		customBulletPrintTime = 4.0
	
	mineralEfficiency = customMineralEfficiency
	remassEfficiency = customRemassEfficiency
	kgps = customKgps
	powerDrawPerKg = customPowerDrawPerKg
	dronePrintTime = customDronePrintTime
	bulletPrintTime = customBulletPrintTime
