EnceladusPOIs
=============

Shows a list of <abbr title="Points of Interest">POIs</abbr> in Enceladus.

![screenshot](https://dump.cy.md/515f67fb8a10a8b382de1a2b7ea14ede/12%3A41%3A16-upload.png)

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/EnceladusPOIs.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.

The list of POIs, and their expirations, are shown on the bottom right of the Dive menu. You can click on an entry to target it.
