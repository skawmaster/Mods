SalaryAlert
===========

Makes the "Crew" button in the Enceladus Prime menu red if there are unpaid staff.

![screenshot](https://dump.cy.md/84d242cb3b113ac81b8d2fe066c5e532/1653388397.449240770.png)

Normally, salaries are paid automatically. However, if you have insufficient cash on an employee's payday, this does not happen. Instead, the employee becomes unhappy and may leave.

If you have all your wealth stored in other assets (mineral market or ships), and you have staff not on the ship crew, it can be easy to miss someone's payday. This mod makes this situation more noticeable.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/SalaryAlert.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.
