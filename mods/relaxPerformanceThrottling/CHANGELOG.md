# Changelog

All notable changes to this project will be documented in this file.

## [0.2] - 2022-07-05
### Added
 - This changelog.

### Changed
 - The config file will now be properly modified if it created by another mod.

## [0.1] - 2022-07-02
### Added
 - Initial release of the mod.